import json
import pickle
import sys

import numpy as np
from keras_preprocessing.sequence import pad_sequences
from tensorflow.keras.layers import Embedding, LSTM, Dense
from tensorflow.keras.models import Sequential
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.utils import to_categorical


def preprocess(dataset):
    import string

    file = open(dataset, "r", encoding="utf8")
    data = file.read()
    data = data.replace('\u201d', '\"').replace('\u201c', '\"').replace('\ufeff', '').replace('\u2019', '\'')

    translator = str.maketrans(string.punctuation, ' ' * len(string.punctuation))  # map punctuation to space
    data = data.translate(translator)
    # print(data)
    data = data.split("\n")
    corpus = list(filter(str.strip, data))
    # print(corpus)
    return corpus


#  Tokenization
#
# encoder = tf.keras.layers.experimental.preprocessing.TextVectorization(
#     max_tokens=VOCAB_SIZE)
# encoder.adapt(data.map(lambda text, label: text))
#

def tokenize(dataset, out_dir):
    tokenizer = Tokenizer()
    tokenizer.fit_on_texts(dataset)

    # saving the tokenizer for predict function.
    pickle.dump(tokenizer, open(out_dir + '/tokenizer1.pkl', 'wb'))
    json.dump(tokenizer.word_index, open(out_dir + '/word_dict.json', 'w'))

    vocab_size = len(tokenizer.word_index) + 1

    sequences = []
    for line in dataset:
        token_list = tokenizer.texts_to_sequences([line])[0]
        for i in range(1, len(token_list)):
            n_gram_sequence = token_list[:i + 1]
            sequences.append(n_gram_sequence)

    return sequences, vocab_size


def pad_and_label(sequences, size):
    # pad sequences
    max_sequence_len = max([len(x) for x in sequences])
    sequences = np.array(pad_sequences(sequences, maxlen=max_sequence_len, padding='pre'))

    # create predictors and label
    xs, labels = sequences[:, :-1], sequences[:, -1]
    ys = to_categorical(labels, num_classes=size, dtype="bool")

    return xs, ys


def preprocess_and_model(dataset, out_dir):
    sequences, size = tokenize(dataset, out_dir)
    xs, ys = pad_and_label(sequences, size)

    model = Sequential()
    model.add(Embedding(size, 10, input_length=1))
    model.add(LSTM(1000, return_sequences=True))
    model.add(LSTM(1000))
    model.add(Dense(1000, activation="relu"))
    model.add(Dense(size, activation="softmax"))
    model.summary()

    # Callbacks

    from tensorflow.keras.callbacks import ModelCheckpoint
    from tensorflow.keras.callbacks import ReduceLROnPlateau
    from tensorflow.keras.callbacks import TensorBoard

    checkpoint = ModelCheckpoint("nnicemodel.h5", monitor='loss', verbose=1,
                                 save_best_only=True, mode='auto')

    reduce = ReduceLROnPlateau(monitor='loss', factor=0.2, patience=3, min_lr=0.0001, verbose=1)

    logdir = 'logsnnicemodel'
    tensorboard_visualization = TensorBoard(log_dir=logdir)

    model.compile(loss="categorical_crossentropy", optimizer=Adam(lr=0.001))
    model.fit(xs, ys, epochs=150, batch_size=64, callbacks=[checkpoint, reduce, tensorboard_visualization])


def main(path, username="user1"):
    input_data = path
    output_folder = username
    clean_data = preprocess(input_data)
    # print(clean_data)
    preprocess_and_model(clean_data, output_folder)


if __name__ == '__main__':
    main(sys.argv[1:])
