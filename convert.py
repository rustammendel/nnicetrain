import sys

import tensorflow as tf
from tensorflow.keras.models import load_model


def convert2tflite(filename, out_dir):
    # Convert the model
    model = load_model(out_dir + filename)

    # Convert the model.
    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    tflite_model = converter.convert()

    # Save the model.
    with open(out_dir + 'model.tflite', 'wb') as f:
        f.write(tflite_model)


if __name__ == '__main__':
    convert2tflite(sys.argv[1], sys.argv[2:])
