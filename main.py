import os
import sys

import convert
import getbook
import procnmodel


def get_books(list_of_books):
    print(list_of_books)
    try:
        print("Downloading books...")
        books_path = "books/"
        if not os.path.exists(books_path):
            os.makedirs(books_path)
        [getbook.download_by_id(bid) for bid in list_of_books]

        merged_file = "merged.txt"
        mf_path = books_path + merged_file

        if os.path.exists(mf_path):
            os.remove(mf_path)

        print("Merging books...")
        for bn in list_of_books:
            bf_name = books_path + bn + ".txt"
            bf = open(bf_name, "r", encoding="utf8")
            data = bf.read()

            with open(mf_path, 'a+') as mf:
                # lines = r.text.encode('UTF-8')
                mf.write(data)
            bf.close()
            os.remove(bf_name)
        mf.close()
        return mf_path

    except Exception as e:
        # return e
        print(e)


def main(list_of_books):
    path = ""
    output_dir = "user1"
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    try:
        path = get_books(list_of_books)
        print("Books Downloaded and merged successfully")
    except Exception as e:
        print(e)
        exit(1)
        # return e
    # ---------------
    try:
        print("Training Model...")
        procnmodel.main(path, output_dir)
    except Exception as e:
        print(e)
        exit(1)
        # return e
    # ---------------

    try:
        print("Converting model")
        convert.convert2tflite("/nnicemodel.h5", output_dir)
    except Exception as e:
        print(e)
        exit(1)

        # return e
    # ---------------

    return "All Done..."


if __name__ == '__main__':
    # booklist = ['65459', '65458', '65456', '65455', '65454']
    main(sys.argv[1:])
    # main(booklist)
